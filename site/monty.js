jQuery(document).ready(function () {
	var count = 100;
	jQuery('#start').click(function () {
		//alert(count);
		simulator(count);
	});
	//jQuery('body').empty();
});

totalWon = 0;
totalWonStaying = 0;
totalWonSwitching = 0;


function simulator(count) {
	var i, d, car, first, monty, second, win, switched;
	var numdoors = 3;
	doorlist = new Object();
	for (d = 0; d < numdoors; d++) {
		doorlist[d] = d + 1;
	}
	for (i = 0; i < count; i++) {
		// put the car somewhere
		car = Math.floor(Math.random() * numdoors);
		carval = doorlist[car];
		// pick a first choice
		first = Math.floor(Math.random() * numdoors);
		firstval = doorlist[first];
		
		// have monty pick a door to open (can't be car or the first choice)
		// make a list of doors that are not the car and not the first choice
		remainingdoors = new Object(doorlist);
		montychoicelist = new Object(doorlist);
		// remove the car from list of choices
		delete montychoicelist[car];
		delete montychoicelist[first];
		montykeys = Object.keys(montychoicelist);
		// monty picks from the list
		monty = montykeys[Math.floor(Math.random() * montykeys.length)];
		montyval = doorlist[monty];
		
		// pick the second choice from remaining doors
		delete remainingdoors[monty];
		remainingkeys = Object.keys(remainingdoors);
		second = remainingkeys[Math.floor(Math.random() * remainingkeys.length)];
		secondval = doorlist[second];
		
		// did we win?
		win = carval == secondval;
		// did we switch?
		switched = firstval != secondval;
		
		// report results
		var log = jQuery('#log>tbody');
		log.append('<tr><td>' + carval + '</td><td>' + firstval + '</td><td>' + montyval + '</td><td>' + secondval + '</td><td>' + win + '</td><td>' + switched + '</td></tr>');
		
		if (win) {
			totalWon++;
			if (switched) {
				totalWonSwitching++;
			} else {
				totalWonStaying++;
			}
			jQuery('#totalWon').text(totalWon);
			jQuery('#totalWonSwitching').text(totalWonSwitching);
			jQuery('#totalWonStaying').text(totalWonStaying);
		}
	}
}
